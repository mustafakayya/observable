package com.kayya.obsarvable;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.kayya.obsarvable.model.Profile;

public class MainActivity extends AppCompatActivity {


    Profile mProfile=new Profile("mustafa","mustafa@brickly.io");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    public Profile getmProfile() {
        return mProfile;
    }
}
