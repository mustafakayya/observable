package com.kayya.obsarvable.model;

import java.util.Observable;

/**
 * Created by mustafakaya on 24/08/16.
 */

public class Profile extends Observable{

    String name;
    String mail;

    public Profile(String name, String mail) {
        this.name = name;
        this.mail = mail;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        notifyObservers(this);
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
        notifyObservers(this);
    }

    public void setData(String name, String mail) {
        this.name = name;
        this.mail = mail;
        setChanged();
        notifyObservers(this);
    }
}
