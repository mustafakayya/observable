package com.kayya.obsarvable.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kayya.obsarvable.MainActivity;
import com.kayya.obsarvable.model.Profile;
import com.kayya.obsarvable.R;

import java.util.Observable;
import java.util.Observer;

/**
 * Created by mustafakaya on 24/08/16.
 */

public class Fragment3 extends Fragment implements Observer{

    TextView profileUrl;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mainView = inflater.inflate(R.layout.fragment3,container,false);
        profileUrl = (TextView)mainView.findViewById(R.id.profileUrl);
        profileUrl.setText(getObservable()!=null ? getObservable().getMail() : "");
        return mainView;
    }

    @Override
    public void update(Observable observable, Object o) {
        if(o!=null && o instanceof Profile){
            profileUrl.setText(((Profile)o).getMail());
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(getObservable()!=null)
            getObservable().addObserver(this);
    }


    @Override
    public void onDetach() {
        super.onDetach();
        if(getObservable()!=null)
            getObservable().deleteObserver(this);
    }

    Profile getObservable(){
        return  ((MainActivity)getActivity()).getmProfile();
    }
}
