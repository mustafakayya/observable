package com.kayya.obsarvable.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kayya.obsarvable.MainActivity;
import com.kayya.obsarvable.model.Profile;
import com.kayya.obsarvable.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Random;

/**
 * Created by mustafakaya on 24/08/16.
 */

public class Fragment1 extends Fragment implements Observer{


    List<String> names = new ArrayList<>(Arrays.asList("Hakkı","Muharrem","Kemal","Melih","Kamil"));
    List<String> photoUrls = new ArrayList<>(Arrays.asList("Hakkı@gmail.com","Muharrem@cgms.com","Kemal@kaya.com","Melih@mucuk.com","Kamil@selam.com"));
    Random rnd =new Random();
    AppCompatButton generateProfile;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mainView = inflater.inflate(R.layout.fragment1,container,false);
        generateProfile = (AppCompatButton)mainView.findViewById(R.id.generateProfile);
        generateProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int randomIndex = rnd.nextInt(names.size());
                getObservable().setData(names.get(randomIndex),photoUrls.get(randomIndex));
            }
        });
        return mainView;
    }

    @Override
    public void update(Observable observable, Object o) {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(getObservable()!=null)
            getObservable().addObserver(this);
    }


    @Override
    public void onDetach() {
        super.onDetach();
        if(getObservable()!=null)
            getObservable().deleteObserver(this);
    }

    Profile getObservable(){
        return  ((MainActivity)getActivity()).getmProfile();
    }

}
