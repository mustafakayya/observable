package com.kayya.obsarvable.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kayya.obsarvable.MainActivity;
import com.kayya.obsarvable.model.Profile;
import com.kayya.obsarvable.R;

import java.util.Observable;
import java.util.Observer;

/**
 * Created by mustafakaya on 24/08/16.
 */

public class Fragment2 extends Fragment implements Observer{

    TextView profileName;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mainView = inflater.inflate(R.layout.fragment2,container,false);
        profileName = (TextView)mainView.findViewById(R.id.profileName);
        profileName.setText(getObservable()!=null ? getObservable().getName() : "");
        return mainView;
    }

    @Override
    public void update(Observable observable, Object o) {
        if(o!=null && o instanceof Profile){
            profileName.setText(((Profile)o).getName());
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(getObservable()!=null)
            getObservable().addObserver(this);
    }


    @Override
    public void onDetach() {
        super.onDetach();
        if(getObservable()!=null)
            getObservable().deleteObserver(this);
    }

    Profile getObservable(){
        return  ((MainActivity)getActivity()).getmProfile();
    }
}
